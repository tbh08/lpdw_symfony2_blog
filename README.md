Blog symfony2
========================

* Renseigner les parametres du serveur mysql dans `app/config/parameters.yml` (`parameters.yml.dist` peut servir d'exemple')
* Lancer un `composer install`
* Installer les assets avec `php app/console assets:install web`
* Créer la bdd `php app/console doctrine:database:create``
* Créer les tables `php app/console doctrine:schema:create`
* Charger les données factices `php app/console doctrine:fixtures:load`