<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()
                   ->getManager();

        $blogs = $em->getRepository('BlogBundle:Blog')
                    ->getLatestBlogs();

        return $this->render('BlogBundle:Page:index.html.twig', array(
            'blogs' => $blogs
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sidebarAction()
    {
        $em = $this->getDoctrine()
                   ->getManager();
    
        $tags = $em->getRepository('BlogBundle:Blog')
                   ->getTags();
    
        $tagWeights = $em->getRepository('BlogBundle:Blog')
                         ->getTagWeights($tags);
    
        $commentLimit   = $this->container
                               ->getParameter('blogger_blog.comments.latest_comment_limit');
        $latestComments = $em->getRepository('BlogBundle:Comment')
                             ->getLatestComments($commentLimit);
    
        return $this->render('BlogBundle:Page:sidebar.html.twig', array(
            'latestComments'    => $latestComments,
            'tags'              => $tagWeights
        ));
    }

}