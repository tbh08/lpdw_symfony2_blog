<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use BlogBundle\Entity\Blog;
use BlogBundle\Form\BlogType;

/**
 * Blog controller.
 */
class BlogController extends Controller
{
    /**
     * Show a blog entry
     * @param $id
     * @param $slug
     * @return Response
     */
    public function showAction($id, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('BlogBundle:Blog')->find($id);

        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog post.');
        }
        
        $comments = $em->getRepository('BlogBundle:Comment')
                       ->getCommentsForBlog($blog->getId());
        
        return $this->render('BlogBundle:Blog:show.html.twig', array(
            'blog'      => $blog,
            'comments'  => $comments
        ));
    }

    /**
     * Create a blog entry
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $blog = new Blog();
        $form = $this->createForm(new BlogType(), $blog);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blog);
            $em->flush();
            return $this->redirectToRoute('BlogBundle_blog_show', ['id' => $blog->getId()]);
        }

        return $this->render('BlogBundle:Blog:new.html.twig', [
            'form' => $form->createView()
        ]);

        //return $this->render('BlogBundle:Post:new.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $blog  = new Blog();
        $request = $this->getRequest();
        $form    = $this->createForm(new BlogType(), $blog);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()
                ->getManager();
            $em->persist($blog);
            $em->flush();

            return $this->redirect($this->generateUrl('BlogBundle_blog_show', array(
                    'id'    => $blog->getId(),
                    'slug'  => $blog->getSlug()
                ))
            );
        }

        return $this->render('BlogBundle:Blog:create.html.twig', array(
            'blog' => $blog,
            'form'    => $form->createView()
        ));
    }

    /**
     * @return Response
     */
    public function listAction()
    {
        $em = $this->getDoctrine()
            ->getManager();

        $blogs = $em->getRepository('BlogBundle:Blog')
            ->getLatestBlogs();

        return $this->render('BlogBundle:Blog:list.html.twig', array(
            'blogs' => $blogs
        ));
    }
}