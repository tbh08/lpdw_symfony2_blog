<?php

namespace BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use BlogBundle\Entity\Comment;
use BlogBundle\Entity\Blog;

class CommentFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $comment = new Comment();
        $comment->setUser('Admin');
        $comment->setComment('Oh j\'adore le bacon, je pourrais en manger toute la journée');
        $comment->setBlog($manager->merge($this->getReference('blog-1')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('BaconLover');
        $comment->setComment('LOLOLOL xD PTDR ROFLCOPTER MOA OSSI XDDDDDDDDDDD');
        $comment->setBlog($manager->merge($this->getReference('blog-1')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('GlaDOS');
        $comment->setComment('This cake is great; it\'s so delicious and moist.');
        $comment->setBlog($manager->merge($this->getReference('blog-2')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Space Core');
        $comment->setComment('I\'m in space!');
        $comment->setBlog($manager->merge($this->getReference('blog-2')));
        $comment->setCreated(new \DateTime("2014-06-12 10:20:11"));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Wheatley');
        $comment->setComment('Hello Guv\'. Neurotoxin inspector, need to shut this place down for a moment. Here\'s my credentials; shut yourself down. I am totally legit, from the board of international neurotoxin, uh, observers, from the United Arab Emirates.');
        $comment->setBlog($manager->merge($this->getReference('blog-2')));
        $comment->setCreated(new \DateTime("2014-06-13 10:20:11"));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Space Core');
        $comment->setComment('Spaaaaaaaaace!');
        $comment->setBlog($manager->merge($this->getReference('blog-2')));
        $comment->setCreated(new \DateTime("2014-06-13 10:24:11"));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Wheatley');
        $comment->setComment('I know you are, mate! Yep. We\'re both in space.');
        $comment->setBlog($manager->merge($this->getReference('blog-2')));
        $comment->setCreated(new \DateTime("2014-06-14 12:20:11"));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('LatinGuy');
        $comment->setComment('Vivamus feugiat dolor nibh, vitae luctus lectus laoreet vitae. Vivamus ultricies');
        $comment->setBlog($manager->merge($this->getReference('blog-3')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('LeRomain29');
        $comment->setComment('Ut efficitur est aliquet hendrerit vulputate. Vestibulum porttitor');
        $comment->setBlog($manager->merge($this->getReference('blog-3')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Anonymous');
        $comment->setComment('Comment on change de pseudo ? J\'arrive pas à accéder à mon profile');
        $comment->setBlog($manager->merge($this->getReference('blog-5')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Admin');
        $comment->setComment('Désolé User38 la fonctionnalitée n\'est pas totalement intégrée, en attendant tu peux définir ton pseudo lorque tu publies un commentaire');
        $comment->setBlog($manager->merge($this->getReference('blog-5')));
        $manager->persist($comment);

        $comment = new Comment();
        $comment->setUser('Anonymous');
        $comment->setComment('Dac merci ! :)');
        $comment->setBlog($manager->merge($this->getReference('blog-5')));
        $manager->persist($comment);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
