<?php

namespace BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use BlogBundle\Entity\Blog;

class BlogFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
        $blog1 = new Blog();
        $blog1->setTitle('Bacon ipsum');
        $blog1->setBlog('<p>Bacon ipsum dolor amet frankfurter short loin meatball shank pork. Pig shankle ground round pork loin. Landjaeger fatback kielbasa doner, andouille picanha sausage tri-tip meatball flank cupim. Chuck hamburger corned beef pastrami andouille biltong prosciutto turducken pig pork meatball.Jowl ham hock t-bone meatball boudin pork loin, rump doner prosciutto andouille beef ribs kielbasa.</p><p>Alcatra cow capicola pig picanha pastrami, short ribs shankle. Alcatra tongue pastrami fatback tenderloin beef ribs, short ribs shoulder venison biltong swine. Beef ribs t-bone bacon, venison picanha shank meatball jerky biltong kevin brisket shankle. Shankle turkey prosciutto boudin. Jerky leberkas pastrami swine rump porchetta cupim short ribs spare ribs flank tongue hamburger. Short loin sausage beef turkey corned beef pork chop.</p>');
        $blog1->setImage('default.jpg');
        $blog1->setAuthor('Me');
        $blog1->setTags('symfony2, bacon');
        $blog1->setCreated(new \DateTime());
        $blog1->setpublishDate(new \DateTime());
        $blog1->setUpdated($blog1->getCreated());
        $manager->persist($blog1);

        $blog2 = new Blog();
        $blog2->setTitle('The cake is a lie');
        $blog2->setBlog('<p>Pig jerky ham hock pork loin filet mignon pork belly fatback venison kielbasa chuck. Landjaeger cow tenderloin andouille biltong pork. Tenderloin jerky bacon tail short loin, kevin prosciutto sirloin cow turducken picanha ball tip ground round. Strip steak boudin venison doner shoulder, meatloaf beef ribs fatback pork loin drumstick short loin. Corned beef meatball alcatra pork chop, picanha cow shankle brisket ham hock landjaeger.</p><p>Spare ribs tri-tip sirloin flank corned beef pork belly kielbasa. Landjaeger picanha bacon pancetta leberkas tenderloin, frankfurter cow kevin flank meatloaf cupim. Drumstick ground round bresaola, tenderloin bacon strip steak meatball t-bone leberkas meatloaf fatback corned beef beef ribs filet mignon turkey. Rump bresaola sausage chuck short ribs brisket shoulder prosciutto pig turkey. Leberkas ground round pig, capicola picanha pork loin short loin porchetta bresaola shoulder bacon drumstick sausage.</p>');
        $blog2->setImage('default.jpg');
        $blog2->setAuthor('You');
        $blog2->setTags('cake, valve, portal, pig');
        $blog2->setCreated(new \DateTime("2014-06-12 10:10:36"));
        $blog2->setpublishDate(new \DateTime());
        $blog2->setUpdated($blog2->getCreated());
        $manager->persist($blog2);

        $blog3 = new Blog();
        $blog3->setTitle('I don\'t want to go');
        $blog3->setBlog('We will sing to you, Doctor. The universe will sing you to your sleep. This song is ending, but the story never ends. Aliquam eu ex ac purus commodo laoreet non sit amet mi. Vivamus feugiat dolor nibh, vitae luctus lectus laoreet vitae. Vivamus ultricies venenatis congue. Maecenas ligula nisl, tempus a quam in, luctus congue nisl. Fusce vel elit nunc. Nulla facilisi. Mauris luctus nulla euismod sem tristique maximus. Ut efficitur est aliquet hendrerit vulputate. Vestibulum porttitor, tellus ornare sollicitudin gravida, urna neque cursus tellus, non malesuada magna massa quis nisl.');
        $blog3->setImage('default.jpg');
        $blog3->setAuthor('david10');
        $blog3->setTags('doctor, screwdriver, police, box');
        $blog3->setCreated(new \DateTime("2014-07-12 12:13:05"));
        $blog3->setpublishDate(new \DateTime("2014-07-16 11:14:06"));
        $blog3->setUpdated($blog3->getCreated());
        $manager->persist($blog3);

        $blog4 = new Blog();
        $blog4->setTitle('Rise and shine, Mister Freeman');
        $blog4->setBlog('Rise and shine, Mister Freeman, rise and … shine. Not that I wish … to imply that you have been sleeping on … the job. No one is more deserving of a rest, and all the effort in the world would have gone to waste until … well … let\'s just say your hour has come again. The right man in the wrong place can make all the difference … in the world. So, wake up, Mister Freeman. Wake up and … smell the ashes.');
        $blog4->setImage('lambda.jpg');
        $blog4->setAuthor('G-Man');
        $blog4->setTags('crowbar, gravity, gun, headcrab');
        $blog4->setCreated(new \DateTime("2015-01-02 18:54:12"));
        $blog4->setpublishDate(new \DateTime("2015-01-02 18:54:12"));
        $blog4->setUpdated($blog4->getCreated());
        $manager->persist($blog4);

        $blog5 = new Blog();
        $blog5->setTitle('Why oh why didn\'t I take the BLUE pill?');
        $blog5->setBlog('<p>What is real? How do you define real? If you\'re talking about what you can feel, what you can smell, what you can taste and see, then real is simply electrical signals interpreted by your brain.</p><p>Have you ever had a dream Neo, that you were so sure was real? What if you were unable to wake from that dream? How would you know the difference between the dream world, and the real world?</p>');
        $blog5->setImage('slowmo.jpg');
        $blog5->setAuthor('WhiteRabbit');
        $blog5->setTags('pill, bullet, time, agent, smith');
        $blog5->setCreated(new \DateTime("2016-01-25 20:32:12"));
        $blog5->setpublishDate(new \DateTime("2016-01-25 20:32:12"));
        $blog5->setUpdated($blog5->getCreated());
        $manager->persist($blog5);

        $manager->flush();
        
        $this->addReference('blog-1', $blog1);
        $this->addReference('blog-2', $blog2);
        $this->addReference('blog-3', $blog3);
        $this->addReference('blog-4', $blog4);
        $this->addReference('blog-5', $blog5);
    }
    
    public function getOrder()
    {
        return 1;
    }
}
