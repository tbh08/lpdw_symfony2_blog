<?php

namespace BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
 
class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class)
            ->add('title', TextType::class)
            ->add('blog', TextareaType::class)
            //->add('image', FileType::class)
            ->add('publishDate', DateType::class,array(
            'widget' => 'single_text',
            'html5'=>true
            ))
            ->add('tags', TextType::class)
            //->add('category', ChoiceType::class)
            ->add('submit', SubmitType::class)
        ;
    }

    public function getName()
    {
        return 'blogbundle_blogtype';
    }
}